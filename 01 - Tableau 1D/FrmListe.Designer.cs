﻿namespace _01___Tableau_1D
{
    partial class FrmListe
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstValeurs = new System.Windows.Forms.ListBox();
            this.lblListe = new System.Windows.Forms.Label();
            this.grpSelection = new System.Windows.Forms.GroupBox();
            this.numValeur = new System.Windows.Forms.NumericUpDown();
            this.btnModifier = new System.Windows.Forms.Button();
            this.lblValeur = new System.Windows.Forms.Label();
            this.btnQuitter = new System.Windows.Forms.Button();
            this.lblIndex = new System.Windows.Forms.Label();
            this.txtIndex = new System.Windows.Forms.TextBox();
            this.grpSelection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numValeur)).BeginInit();
            this.SuspendLayout();
            // 
            // lstValeurs
            // 
            this.lstValeurs.FormattingEnabled = true;
            this.lstValeurs.Location = new System.Drawing.Point(12, 25);
            this.lstValeurs.Name = "lstValeurs";
            this.lstValeurs.Size = new System.Drawing.Size(120, 95);
            this.lstValeurs.TabIndex = 0;
            this.lstValeurs.SelectedIndexChanged += new System.EventHandler(this.lstValeurs_SelectedIndexChanged);
            // 
            // lblListe
            // 
            this.lblListe.AutoSize = true;
            this.lblListe.Location = new System.Drawing.Point(12, 9);
            this.lblListe.Name = "lblListe";
            this.lblListe.Size = new System.Drawing.Size(86, 13);
            this.lblListe.TabIndex = 1;
            this.lblListe.Text = "Liste des valeurs";
            // 
            // grpSelection
            // 
            this.grpSelection.Controls.Add(this.txtIndex);
            this.grpSelection.Controls.Add(this.lblIndex);
            this.grpSelection.Controls.Add(this.numValeur);
            this.grpSelection.Controls.Add(this.btnModifier);
            this.grpSelection.Controls.Add(this.lblValeur);
            this.grpSelection.Location = new System.Drawing.Point(138, 20);
            this.grpSelection.Name = "grpSelection";
            this.grpSelection.Size = new System.Drawing.Size(215, 71);
            this.grpSelection.TabIndex = 2;
            this.grpSelection.TabStop = false;
            this.grpSelection.Text = "Selection";
            // 
            // numValeur
            // 
            this.numValeur.Location = new System.Drawing.Point(65, 45);
            this.numValeur.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numValeur.Name = "numValeur";
            this.numValeur.Size = new System.Drawing.Size(63, 20);
            this.numValeur.TabIndex = 3;
            this.numValeur.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numValeur.ThousandsSeparator = true;
            // 
            // btnModifier
            // 
            this.btnModifier.Location = new System.Drawing.Point(134, 42);
            this.btnModifier.Name = "btnModifier";
            this.btnModifier.Size = new System.Drawing.Size(75, 23);
            this.btnModifier.TabIndex = 4;
            this.btnModifier.Text = "Modifier";
            this.btnModifier.UseVisualStyleBackColor = true;
            this.btnModifier.Click += new System.EventHandler(this.btnModifier_Click);
            // 
            // lblValeur
            // 
            this.lblValeur.AutoSize = true;
            this.lblValeur.Location = new System.Drawing.Point(16, 47);
            this.lblValeur.Name = "lblValeur";
            this.lblValeur.Size = new System.Drawing.Size(43, 13);
            this.lblValeur.TabIndex = 2;
            this.lblValeur.Text = "Valeur :";
            // 
            // btnQuitter
            // 
            this.btnQuitter.Location = new System.Drawing.Point(278, 97);
            this.btnQuitter.Name = "btnQuitter";
            this.btnQuitter.Size = new System.Drawing.Size(75, 23);
            this.btnQuitter.TabIndex = 3;
            this.btnQuitter.Text = "Quitter";
            this.btnQuitter.UseVisualStyleBackColor = true;
            this.btnQuitter.Click += new System.EventHandler(this.btnQuitter_Click);
            // 
            // lblIndex
            // 
            this.lblIndex.AutoSize = true;
            this.lblIndex.Location = new System.Drawing.Point(16, 22);
            this.lblIndex.Name = "lblIndex";
            this.lblIndex.Size = new System.Drawing.Size(39, 13);
            this.lblIndex.TabIndex = 0;
            this.lblIndex.Text = "Index :";
            // 
            // txtIndex
            // 
            this.txtIndex.Location = new System.Drawing.Point(65, 19);
            this.txtIndex.Name = "txtIndex";
            this.txtIndex.ReadOnly = true;
            this.txtIndex.Size = new System.Drawing.Size(63, 20);
            this.txtIndex.TabIndex = 1;
            this.txtIndex.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // FrmListe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(370, 134);
            this.Controls.Add(this.btnQuitter);
            this.Controls.Add(this.grpSelection);
            this.Controls.Add(this.lblListe);
            this.Controls.Add(this.lstValeurs);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmListe";
            this.Text = "Liste de valeurs";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.grpSelection.ResumeLayout(false);
            this.grpSelection.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numValeur)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstValeurs;
        private System.Windows.Forms.Label lblListe;
        private System.Windows.Forms.GroupBox grpSelection;
        private System.Windows.Forms.Label lblValeur;
        private System.Windows.Forms.Button btnModifier;
        private System.Windows.Forms.NumericUpDown numValeur;
        private System.Windows.Forms.Button btnQuitter;
        private System.Windows.Forms.TextBox txtIndex;
        private System.Windows.Forms.Label lblIndex;
    }
}

