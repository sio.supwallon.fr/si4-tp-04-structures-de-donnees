﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _01___Tableau_1D
{
    public partial class FrmListe : Form
    {
        // todo 01 : Déclarer un tableau d'entier appelé "valeurs"
        //           Déclarer le tableau ici, au début de la déclaration de la classe
        //           Cette variable sera "globale" à l'ensemble de la classe
        //           c'est à dire qu'on pourra l'utiliser dans tous les blocs d'instructions qui suivent
        //           c'est assez pratique ... mais il ne faut pas en abuser ;)

        public FrmListe()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // todo 02 : Allouer un tableau de 10 entier et l'affecter à "valeurs"

            // todo 03 : Donner des valeurs aux 10 cases (peu importe quelle valeur)

            // todo 04 : Affecter le tableau à la propriété DataSource de la listBox lstValues
        }

        private void lstValeurs_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(lstValeurs.SelectedItem != null)
            {
                // todo 05 : Affecter la propriété SelectedIndex de lstValeurs au composant txtIndex
                //           Attention txtIndex est une TextBox, il faut donc convertir l'index en string
                //           pour cela, on va utiliser la fonctionnalité ToString() disponible directement
                //           à partir de la propriété SelectedIndex


                // todo 06 : Affecter la propriété SelectedItem de lstValeurs au composant numValeur
                //           Attention SelectedItem va fournir l'élément sélectionné sous forme d'un "objet"
                //           son type n'est pas connu par lstValeur, c'est à nous de lui dire qu'il s'agit d'un int
                //           pour cela, on va forcer le changement de type manuellement en ajoutant juste après le =
                //           le type entre parenthèse. 
                //           Exemple a = (int)b;
                //           Cette opération d'appelle un "transtypage" (ou un "cast" en anglais)

            }
        }

        private void btnModifier_Click(object sender, EventArgs e)
        {
            // todo 07 : Déclarer la variabler entière index

            // toto 08 : Donner à index la valeur fournie par la propriété SelectedIndex de lstValeurs

            // todo 09 : Affecter la valeur du composant numValeur à la case correspondant à l'index du tableau valeurs
            //           Attention la valeur fournie par numValeur est de type decimal,
            //           il faut le "caster" en int pour pouvoir l'affecter dans le tableau

            // todo 10: Raffraichir la ListBox
            //          Petite astuce ... il faut "débrancher" la liste de sa source (valeur null = rien)
            //          et réaffecter ensuite le tableau pour forcer la mise à jour de la listeBox

        }

        private void btnQuitter_Click(object sender, EventArgs e)
        {
            // todo 11 : Quitter le programme


            // Bravo, vous avez terminé ce projet !
        }
    }
}
